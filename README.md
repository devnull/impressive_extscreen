# Simple bash script for impressive PDF-viewer  

#### Introduction
To display beamer (LaTeX) presentations, I use [impressive](http://impressive.sourceforge.net/), a wonderful PDF viewer written in Python and aimed for PDF/beamer slides, but which is a pain in the a\*\* if you use it on an external display that has a different resolution with different aspect ratio than your laptop's screen, especially in extended mode.

This script aims that make executing **impressive** in such settings, by providing presets and infos about your resolution, so you can use **impressive** without specifying all options manually, instead, all you need to do is to execute this script with path to your slides.pdf document as an argument, then choice the appropriate options (interactive menu).

````
me@linuxbox:~$ imprextscreen.sh ~/path/to/beamer_stuff.pdf
````

Don't forget to make it executable when you download it, if it isn't
````
me@linuxbox:~$ chmod u+x imprextscreen.sh
````

#### Features
- Launch impressive with one of preset resolutions
- Launch impressive with an arbitrary resolution (you must specify it in the following format : <width\>x<height\>)
- Show supported resolutions for all attached screens, using [xrandr](https://en.wikipedia.org/wiki/RandR), then lets make a choice
- Show current resolutions for all attached screens, using [xrandr](https://en.wikipedia.org/wiki/RandR), then lets make a choice
- Execute **arandr** (XRandR GUI), then lets make a choice

#### Todo
- Add an option to automatically extract the external screen resolution, and use it when impressive is executed, without user interaction
If I have enough time implement and then to test these features

#### Limitations
This scripts currently works if the external screen position is set at the right of the primary screen, not above, or other uncommon seetings (such external screen ath the left, or below the primary one), to support such case your have have to tweak -g | --geometry option parameters. This options uses the following syntax, where posX and posY define the external screen's relative position, horizontally and vertically.

````
me@linuxbox:~$ impressive -ff -g <width>x<height>[+<posX>+<posY>]
````
My primary screen's width is 1600, if yours is different, you must change posX (sounds like POSIX :) ) values in the case statement.
