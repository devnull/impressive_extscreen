#!/bin/bash
#*******************************************************
# Name: impressive_external_screen.sh                  *
# Author: Dave Null                                    *
# Version: 1.0                                         *
# License: BSD 3-Clause "New" License                  *
#*******************************************************
#set -x # debugging

if [ $# -lt 1 ]
  then
    echo "Please specify path to beamer file (*.pdf)"
    exit 1
fi

if [ $# -eq 2 ]
  then
    duration="$2"
  else
    duration="60m"
fi

file="$1"

printf "Talk duration set to $duration\n"

printf "Please choose a display resolution for your external screen\n"
printf "1 : 1024x768\n"
printf "2 : 1152x864\n"
printf "3 : 1280x800\n"
printf "4 : 1280x1024\n"
printf "5 : 1600x1200\n"
printf "6 : Arbitrary resolution\n"
printf "7 : Show selected display resolutions (xrandr) for all connected screens (primary, then external), then make another choice\n"
printf "8 : Show supported display resolutions (xrandr) for all connected screens, then make another choice\nCurrent resolutions are marked by *\n"
printf "9 : Execute arandr then choose a display resolution\n"

choice()
{
	read select
	case $select in
	1)
	  impressive -d $duration -ff -g 1024x768+1600+0 "$file"
	  ;;
	2)
	  impressive -d $duration -ff -g 1152x864+1600+0 "$file"
	  ;;
	3)
	  impressive -d $duration -ff -g 1280x800+1600+0 "$file"
	  ;;
	4)
	  impressive -d $duration -ff -g 1280x1024+1600+0 "$file"
	  ;;
	5)
	  impressive -d $duration -ff -g 1600x1200+1600+0 "$file"
	  ;;
	6)
	  printf "Specify an arbitrary resolution <width>x<height> - ex: 1024x768\n"
	  read definition
	  impressive -d $duration -ff -g $definition+1600+0 "$file"
	  ;;
	7)
	  printf "Current resolutions for all screens are:\n"
	  xrandr | fgrep '*'
	  printf "Please make another choice\n"
	  choice
	  ;;
	8)
	  printf "Supported resolutions for all screens are:\n"
	  xrandr | grep [0-9].*x[0-9].* # | less # Uncomment "| less" if resolution list is too long
	  printf "Please make another choice\n"
	  choice
	  ;;
	9)
	  nohup arandr >/dev/null 2>&1 &
	  printf "arandr launched, please make another choice when ready\n"
	  choice
	  ;;
	*)
	  printf "Wrong choice, please retry again\n"
	  choice
	  ;;
	esac
}

choice

exit 0
